import socket
import base64                
  
s = socket.socket()          
port = 12352
s.bind(('', port))         


# put the socket into listening mode 
s.listen(5)      
print("socket is listening")



def sum_func(params, flag):

	try:
		if flag == 0:
			value = 0
			for val in params:
				value += val

		else:
			value = ""
			for val in params:
				value += str(val)

	except:
		value = "Error : Can't Add"


	return value

def mul_func(params, flag):

	try:
		if flag == 0:
			value = 1
			for val in params:
				value = int(value) * val

		else:
			value = ""
			for val in params:
				value = value * val

	except:
		value = "Error : Strings Can't Multiply"


	return value



def process_funcn(funcn_name, params, flag):

	value = globals()[funcn_name](params, flag)
	# if funcn_name == "sum_func":
	# 	value = sum_func(params, flag)

	# if funcn_name == "mul_func":
	# 	value = mul_func(params, flag)

	return str(value) 




def processing(request):

	funcn_name = request.split("$")[0]
	params = request.split("$")[1].split("&")
	params_type = request.split("$")[2].split("^")
	
	transformed_params = []
	flag = 0

	for val in range(0, len(params_type)):
		if params_type[val] == "int":
			transformed_params.append(int(params[val]))

		if params_type[val] == "float":
			transformed_params.append(float(params[val]))

		if params_type[val] == "str":
			flag = 1
			transformed_params.append(str(params[val]))


	result = process_funcn(funcn_name, transformed_params, flag)

	# Final Data
	data_bytes = result.encode("utf-8")
	encoded = base64.b64encode(data_bytes)

	return encoded




while True: 
	c, addr = s.accept()      
	print('Got connection from', addr)
  
	# send a thank you message to the client.  
	data = c.recv(4096)
	val = base64.b64decode(data)
	request = val.decode("utf-8")

	encoded = processing(request)
	c.send(encoded)


	c.close() 
