import socket                
import base64
import re


def Get_Result(query, server_data):

	s = socket.socket()          
	port = 12352 
	s.connect(('127.0.0.1', port)) 

	data_bytes = server_data.encode("utf-8")
	encoded_url = base64.b64encode(data_bytes)
	s.send(encoded_url)

	result = s.recv(1024)
	val = base64.b64decode(result)
	ans = val.decode("utf-8")

	print("Result of ",query," is ",ans)
	print("\n")

	s.close() 




def process_funcn(data):
	funcn_name = data.split("(")[0]

	char1 = '('
	char2 = ')'
	params = data[data.find(char1)+1 : data.find(char2)]

	params_list = params.split(",")
	params_count = len(params_list)
	params_server = ""
	params_type = ""

	print("Enter "+str(params_count)+" value")
	for i in range(0, params_count):
		x = input("Enter Value : ")
		params_server += str(x)

		if x.isalpha():
			params_type += "str"

		elif x.isdigit():
			params_type += "int"
		
		else:
			params_type += "float"

		
		if i != params_count-1:
			params_server += "&"
			params_type += "^"

	server_data = funcn_name + "$" + params_server + "$" + params_type

	Get_Result(data, server_data)



def processing_file():
	with open('input.txt', 'r') as file:
		data = ""
		data = file.read().replace('\n', ' ')

	for val in data.split(" "):
		process_funcn(val)

processing_file()